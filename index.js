const csvFilePath = './data/matches.csv'; // Replace with the path to your CSV file
const jsonFilePath = './data/matches.json'; // Replace with the path where you want to save the JSON output

const csv = require('csvtojson');
const fs = require('fs');

csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    
    fs.writeFile(jsonFilePath, JSON.stringify(jsonObj, null, 2), (err) => {
      if (err) {
        console.error('Error writing JSON file:', err);
      } else {
        console.log('CSV to JSON conversion complete!');
      }
    });
  })
  .catch((err) => {
    console.error('Error converting CSV to JSON:', err);
  });
