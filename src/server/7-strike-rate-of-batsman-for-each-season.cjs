const csvFilePath = "../data/deliveries.csv"; // Replace with the path to your CSV file
const csvFilePath1 = "../data/matches.csv";
const jsonFilePath =
  "../public/output/7-strike-rate-of-batsman-for-each-season.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function strikeRate() {
  Promise.all([csv().fromFile(csvFilePath), csv().fromFile(csvFilePath1)])
    .then(([deliveries, matches]) => {
      const match_ids = matches.map((match) => {
        return { id: match.id, season: match.season };
      });

      const mergedDataset = deliveries.map((delivery) => {
        const year = match_ids.find((match) => {
          return match.id === String(delivery.match_id);
        });

        return { ...delivery, Year: year.season };
      });

      // console.log(mergedDataset)

      const batsmen_data = mergedDataset.reduce((acc, deliveries) => {
        const batsman = deliveries.batsman;
        const runs = Number(deliveries.batsman_runs);
        const season = deliveries.Year;

        if (!acc[batsman]) {
          acc[batsman] = {};
        }
        if (!acc[batsman][season]) {
          acc[batsman][season] = {
            runs_scored: runs,
            balls_faced: 1,
            strike_rate: undefined,
          };
        } else {
          if (!acc[batsman][season].strike_rate) {
            acc[batsman][season].strike_rate =
              (acc[batsman][season].runs_scored /
                acc[batsman][season].balls_faced) *
              100;
          }
          acc[batsman][season].runs_scored += runs;
          acc[batsman][season].balls_faced++;
          acc[batsman][season].strike_rate =
            (acc[batsman][season].runs_scored /
              acc[batsman][season].balls_faced) *
            100;
        }

        return acc;
      }, {});

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(batsmen_data, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
strikeRate();
