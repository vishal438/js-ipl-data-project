const csvFilePath = "../data/matches.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/1-matches-per-year.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function matchesPerYear() {
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      const matches_data = jsonObj;
      const matchesPerYear = matches_data.reduce(
        (acc, curr, currentIndex, matches_data) => {
          const match = curr["season"];
          if (acc[match]) {
            acc[match]++;
          } else {
            acc[match] = 1;
          }
          return acc;
        },
        {}
      );

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(matchesPerYear, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
matchesPerYear();
