const csvFilePath = "../data/deliveries.csv";
const csvFilePath1 = "../data/matches.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/3-extra-run-conceded-in-2016.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function extraRunConceded() {
  Promise.all([csv().fromFile(csvFilePath), csv().fromFile(csvFilePath1)])
    .then(([deliveriesData, matchesData]) => {
      // Filter the matches that occurred in the year 2016
      const matchesIn2016 = matchesData.filter((match) => {
        return match.season === "2016";
      });

      // Get match IDs for 2016 matches
      const matchIdsIn2016 = matchesIn2016.map((match) => {
        return match.id;
      });

      const extraRunsConceded = deliveriesData.reduce((acc, delivery) => {
        const matchId = delivery.match_id;
        const bowlingTeam = delivery.bowling_team;
        const extraRuns = parseInt(delivery.extra_runs);

        if (matchIdsIn2016.includes(matchId)) {
          if (!acc[bowlingTeam]) {
            acc[bowlingTeam] = extraRuns;
          } else {
            acc[bowlingTeam] += extraRuns;
          }
        }

        return acc;
      }, {});

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(extraRunsConceded, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
extraRunConceded();
