const csvFilePath = "../data/deliveries.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/8-highest-number-of-times-player-dismissed.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function playerDismissed() {
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      const deliveries = jsonObj;
      const p1_dismiss_p2_max = deliveries.reduce((acc, delivery) => {
        const player1 = delivery.bowler;
        const player2 = delivery.player_dismissed;
        const key = String(player1 + " dismissed " + player2);
      
        if (player2 != undefined && player2 != null && player2.trim().length > 0) {
          if (!acc[key]) {
            acc[key] = 1;
          } else {
            acc[key]++;
          }
        }
        return acc;
      }, {});
      
      const maxDismissInfo = Object.keys(p1_dismiss_p2_max).reduce((maxDismissInfo, key) => {
        if (Number(p1_dismiss_p2_max[key]) > maxDismissInfo.max_dismiss_count) {
          return {
            max_dismiss_count: p1_dismiss_p2_max[key],
            max_dismiss: `${key} ${p1_dismiss_p2_max[key]}`,
          };
        } else {
          return maxDismissInfo;
        }
      }, { max_dismiss_count: 0, max_dismiss: '' });
      
      const res = {};
      
      res['total_dissmisal'] = p1_dismiss_p2_max;
      res['max_dismiss'] = maxDismissInfo.max_dismiss;
      

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(res, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
playerDismissed();
