const csvFilePath = "../data/matches.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/6-player-of-match.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function playerOfMatch() {
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      const matches = jsonObj;
      const playerOfTheMatchPerSeason = matches.reduce((acc, elem) => {
        const { season, player_of_match } = elem;
        acc[season] = acc[season] || {};
        acc[season][player_of_match] = (acc[season][player_of_match] || 0) + 1;
        return acc;
      }, {});

      const result = Object.keys(playerOfTheMatchPerSeason).reduce(
        (acc, season) => {
          const playerObj = playerOfTheMatchPerSeason[season];
          const sortedPlayers = Object.entries(playerObj).sort(
            (a, b) => b[1] - a[1]
          );
          const [key, value] = sortedPlayers[0];
          acc[season] = { [key]: value };
          return acc;
        },
        {}
      );

      fs.writeFile(jsonFilePath, JSON.stringify(result, null, 2), (err) => {
        if (err) {
          console.error("Error writing JSON file:", err);
        } else {
          console.log("CSV to JSON conversion complete!");
        }
      });
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
playerOfMatch();
