const csvFilePath = "../data/deliveries.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/9-bowler-with-best-economy-super-over.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function bestEconomySuperOver(){
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    const deliveries=jsonObj
    let superOverEconomy = deliveries.reduce((acc,event)=>{
        if(event.is_super_over != 0){
            if(!acc[event.bowler]){
                acc[event.bowler] = {
                    runs:Number(event.total_runs),
                    balls:1,
                    eco:0
                }
            }else{
                acc[event.bowler].runs += Number(event.total_runs);
                acc[event.bowler].balls += 1;
                let economy = (acc[event.bowler].runs/acc[event.bowler].balls)*6;
                acc[event.bowler].eco = economy;
            }
        }
        return acc;
    },{});
  
    let answer = Object.entries(superOverEconomy);
  
    let superOverEconomyArr = answer.sort((prev, curr) => {
        if (prev[1].eco > curr[1].eco) {
            return 1;
        }
        else if (prev[1].eco < curr[1].eco) {
            return -1;
        }
        else {
            return 0;
        }
    });
    let bestBowler = superOverEconomyArr.slice(0,1);
    

    fs.writeFile(jsonFilePath, JSON.stringify(bestBowler, null, 2), (err) => {
      if (err) {
        console.error("Error writing JSON file:", err);
      } else {
        console.log("CSV to JSON conversion complete!");
      }
    });
  })
  .catch((err) => {
    console.error("Error converting CSV to JSON:", err);
  });
}

bestEconomySuperOver()
