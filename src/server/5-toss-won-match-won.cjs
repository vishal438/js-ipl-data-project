const csvFilePath = "../data/matches.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/5-toss-won-match-won.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function tossWonMatchWon() {
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      const matches= jsonObj;
    

      let match = matches.map((match) => {
        return {
            team: match['winner'],
            toss: match['toss_winner']
        }
    })

    let result = match.reduce((accum, element) => {
        if(element.team === element.toss){
            accum[element.team] = (accum[element.team] ?? 0) + 1;
        }

        return accum
    }, {});

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(result, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
tossWonMatchWon();
