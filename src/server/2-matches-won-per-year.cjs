const csvFilePath = "../data/matches.csv"; // Replace with the path to your CSV file
const jsonFilePath = "../public/output/2-matches-per-year.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");
function matchesWonPerYear() {
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      const matches = jsonObj;

      const matchesWon = matches.reduce((acc, match) => {
        const season = match.season;
        const winner = match.winner;

        if (!acc[season]) {
          acc[season] = { [winner]: 1 };
        } else {
          if (!acc[season][winner]) {
            acc[season][winner] = 1;
          } else {
            acc[season][winner] += 1;
          }
        }

        return acc;
      }, {});

      fs.writeFile(jsonFilePath, JSON.stringify(matchesWon, null, 2), (err) => {
        if (err) {
          console.error("Error writing JSON file:", err);
        } else {
          console.log("CSV to JSON conversion complete!");
        }
      });
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}

matchesWonPerYear();
