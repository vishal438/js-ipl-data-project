const csvFilePath = "../data/deliveries.csv"; // Replace with the path to your CSV file
const csvFilePath1 = "../data/matches.csv";
const jsonFilePath = "../public/output/4-top-10-economical-bowlers-2015.json"; // Replace with the path where you want to save the JSON output

const csv = require("csvtojson");
const fs = require("fs");

function topEconomicalBowlers() {
  Promise.all([csv().fromFile(csvFilePath), csv().fromFile(csvFilePath1)])
    .then(([deliveries, matches]) => {
      const years = matches.filter((match) => {
        return match.season === "2015";
      });

      const matchId = years.map((match) => match.id);

      const deliveriesIn2015 = deliveries.filter((delivery) =>
        matchId.includes(delivery.match_id)
      );

      const result = deliveriesIn2015.reduce((acc, elem) => {
        const bowler = elem.bowler;

        if (acc[bowler] === undefined) {
          acc[bowler] = {
            runs: Number(elem.total_runs),
            balls: 1,
            economy: undefined,
          };
        } else {
          acc[bowler].runs += Number(elem.total_runs);
          acc[bowler].balls++;
          acc[bowler].economy = (acc[bowler].runs / acc[bowler].balls) * 6;
        }

        return acc;
      }, {});

      const topBowlers = Object.entries(result)
        .sort((prev, curr) => {
          return prev[1].economy - curr[1].economy;
        })
        .slice(0, 10);

      const top10Bowlers = Object.fromEntries(topBowlers);

      fs.writeFile(
        jsonFilePath,
        JSON.stringify(top10Bowlers, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing JSON file:", err);
          } else {
            console.log("CSV to JSON conversion complete!");
          }
        }
      );
    })
    .catch((err) => {
      console.error("Error converting CSV to JSON:", err);
    });
}
topEconomicalBowlers();
